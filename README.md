# Google Drive Clone App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app)
and deployed with Firebase. 

You can check functionality of project with [this link](https://drive-7d217.web.app)

## Functionality

- Google Auth
- Adding Files by pressing "+ New" button in the upper left corner
- Deleting Files by pressing trash bin Icon 
- Files distinction based on the logged in user
