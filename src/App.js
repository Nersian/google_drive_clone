import {useState} from 'react'
import './App.css';
import FileView from './components/fileView/FileView';
import Header from './components/header'
import Sidebar from './components/sidebar'
import SideIcons from './components/sideIcons'
import GDriveLogo from './media/google_drive_icon.svg'

import {auth, provider} from './firebase'

function App() {
  const [user, setUser] = useState()


  const handleLogin = () =>{
    if (!user) {
      auth.signInWithPopup(provider).then((result) => {
        setUser(result.user)
      })
    }
  }
  return (
    <div className="App">
      {
        user ? (
          <>
          <Header userPhoto={user.photoURL}/>
          <div className="app__main">
            <Sidebar
              user_id = {user.uid}
            />
            <FileView
              user_id = {user.uid}
            />
            <SideIcons/>
          </div>
          </>
        ):(
          <div className="app__login">
            <img src={GDriveLogo} alt="GoogleDrive" />
            <button onClick={handleLogin}>Log in to Google Drive</button>  
          </div>
        )
      }

      

    </div>
  );
}

export default App;
