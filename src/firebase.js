import firebase from 'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyCL3JuM2SNcEf2MwamfMC7uC7LIc73FFfA",
    authDomain: "drive-7d217.firebaseapp.com",
    projectId: "drive-7d217",
    storageBucket: "drive-7d217.appspot.com",
    messagingSenderId: "216866814989",
    appId: "1:216866814989:web:b8515b34bba3c4d9026944",
    measurementId: "G-WN4J6MZ81Y"
  };

const firebaseApp = firebase.initializeApp(firebaseConfig)

const auth = firebase.auth()
const provider = new firebase.auth.GoogleAuthProvider()
const storage = firebase.storage()
const db = firebaseApp.firestore()

export {auth, provider, storage, db}