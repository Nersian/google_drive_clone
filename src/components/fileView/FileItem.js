import React from 'react'
import '../../styles/FileItem.css'

import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import DeleteIcon from '@material-ui/icons/Delete';
import {db} from '../../firebase'

const monthShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

const FileItem = ({ id, caption, timestamp, fileUrl, size, user_id }) => {
    const fileDate = `${timestamp?.toDate().getDate()} ${monthShort[timestamp?.toDate().getMonth()]} ${timestamp?.toDate().getFullYear()}`

    const getFileSizeString = (fileSizeInBytes) => {
        let i = -1;
        const byteUnits = ['kB', 'MB', 'GB'];
        do {
            fileSizeInBytes = fileSizeInBytes / 1024;
            i++;
        } while (fileSizeInBytes > 1024);

        return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];
    };

    const handleDeletion = () =>{
        db.collection(user_id).where('caption', '==', caption).get().then(querySnapshot => {
            querySnapshot.docs[0].ref.delete();
        }
        )};

    return (
        <div className='fileItem'> 
            <div className="fileItem__link">
                <a href={fileUrl} target='_blank' download>
                    <div className="fileItem--left">
                        <InsertDriveFileIcon/>
                        <p>{caption}</p>
                    </div>
                    <div className="fileItem--right">
                        <p>{fileDate}</p>
                        <p>{getFileSizeString(size)}</p>
                    </div>
                </a>
            </div>
            <div className="fileItem__delete">
                <DeleteIcon
                    onClick={handleDeletion}
                />
            </div>
        </div>
    )
}

export default FileItem
