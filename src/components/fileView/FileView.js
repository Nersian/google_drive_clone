import React, { useEffect, useState, useLayoutEffect } from 'react'
import {db} from '../../firebase'
import FileItem from './FileItem'
import '../../styles/FileView.css'
import FileCard from './FileCard'

const FileView = ({ user_id }) => {
    const [files, setFiles] = useState([])
    const cardWidth = 250;
    const sideBarsWidth = 300; 
    const [numCardElements, setNumCardElements] = useState(1);
        
    useLayoutEffect(() => {
        function updateNumCardElements() {
        let width = window.innerWidth;
        let cardNumber = Math.floor(Math.max((width - sideBarsWidth)/cardWidth,1))
        setNumCardElements(cardNumber);
        }
        window.addEventListener('resize', updateNumCardElements);
        updateNumCardElements();
        return () => window.removeEventListener('resize', updateNumCardElements);
    }, []);


    useEffect(() => {
        db.collection(user_id).onSnapshot(snapshot => {
            setFiles(snapshot.docs.map(doc => ({
                id: doc.id,
                item: doc.data()
            })))
        }) 
    }, [])


    return (
        <div className="fileView">
            <div className="fileView__row">
                {

                    files.slice(0,numCardElements).map(({id, item})=>(
                        <FileCard 
                            name={item.caption}
                        />
                    ))
                }
            </div>

            <div className="fileView__titles">
                <div className="fileView__titles--left">
                    <p>Name</p>
                </div>
                <div className="fileView__titles--right">
                    <p>Last Modified</p>
                    <p>Files size</p>
                </div>
            </div>
            {
                files.map(({id, item}) => (
                    <>
                    <FileItem 
                        id={id} 
                        caption={item.caption} 
                        timestamp={item.timestamp} 
                        fileUrl={item.fileUrl} 
                        size={item.size}
                        user_id={user_id}
                    />
                    </>
                ))
            }
        </div>
    )
}

export default FileView
