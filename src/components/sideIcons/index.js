import React from 'react'
import AddIcon from '@material-ui/icons/Add';
import '../../styles/SideIcons.css'

const index = () => {
    return (
        <div className='sideIcons'>
            <div className="sideIcons__top">
                <img src="https://cdn4.iconfinder.com/data/icons/logos-brands-in-colors/48/google-calendar-512.png" alt="CalendarIcon"/>
                <img src="https://w7.pngwing.com/pngs/278/649/png-transparent-google-keep-icon-symbol-brand-yellow-keep-rectangle-orange-android.png" alt="KeepIcon"/>
                <img src="https://image.pngaaa.com/566/2512566-middle.png" alt="TaskIcon"/> 
            </div>

            <hr/>

            <div className="sideIcons__plusIcon">
                <AddIcon/>
            </div>
        </div>
    )
}

export default index
