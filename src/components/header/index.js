import React from 'react'
import '../../styles/Header.css'
import GDriveLogo from '../../media/google_drive_icon.svg'
import SearchIcon from '@material-ui/icons/Search';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AppsIcon from '@material-ui/icons/Apps';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import SettingsIcon from '@material-ui/icons/Settings';

const index = ({userPhoto}) => {
    return (
        <div className='header'>
            <div className="header__logo">
                <img src={GDriveLogo} alt=""/>
                <span>Drive</span>
            </div>

            <div className="header__searchContainer">
                <SearchIcon />
                <div className="header__searchBar">
                    <input type='text' placeholder='Search in Drive' />
                </div>
                <ExpandMoreIcon />
            </div>

            <div className="header__icons">
                <HelpOutlineIcon />
                <SettingsIcon />
                <AppsIcon />
                <img src={userPhoto} alt="User Photo"/>
            </div>
        </div>
    )
}

export default index
